from django.db import models
from django.utils.text import slugify
from django.contrib.auth.models import AbstractUser


class RightsSupport(models.Model):
    class Meta:

        managed = False  # No database table creation or deletion  \
        # operations will be performed for this model.

        permissions = (

            ("add_user", "Can Create User"),
            ("view_user", "Can View Single User"),
            ("change_user", "Can Change User"),
            ("list_user", "Can View User List"),

            ("add_plan", "Can Create Plan"),
            ("view_plan", "Can View Single Plan"),
            ("change_plan", "Can Change Plan"),
            ("list_plan", "Can View Plan List"),

            ("add_discount", "Can Create Discount"),
            ("view_discount", "Can View Single Discount"),
            ("change_discount", "Can Change Discount"),
            ("list_discount", "Can View Discount List"),

            ("add_payment", "Can Create Payment"),
            ("view_payment", "Can View Single Payment"),
            ("change_payment", "Can Change Payment"),
            ("list_payment", "Can View Payment List"),

            ("add_sms", "Can Create SMS"),
            ("view_sms", "Can View Single SMS"),
            ("change_sms", "Can Change SMS"),
            ("list_sms", "Can View SMS List"),

            ("add_social", "Can Create Social"),
            ("view_social", "Can View Single Social"),
            ("change_social", "Can Change Social"),
            ("list_social", "Can View Social List"),

            ("add_ticket", "Can Create Ticket"),
            ("view_ticket", "Can View Single Ticket"),
            ("change_ticket", "Can Change Ticket"),
            ("list_ticket", "Can View Ticket List"),

            ("add_searched", "Can Create Searched"),
            ("view_searched", "Can View Single Searched"),
            ("change_searched", "Can Change Searched"),
            ("list_searched", "Can View Searched List"),

            ("add_result", "Can Create Result"),
            ("view_result", "Can View Single Result"),
            ("change_result", "Can Change Result"),
            ("list_result", "Can View Result List"),

            ("add_number", "Can Create Number"),
            ("view_number", "Can View Single Number"),
            ("change_number", "Can Change Number"),
            ("list_number", "Can View Number List"),

            ("add_city", "Can Create City"),
            ("view_city", "Can View Single City"),
            ("change_city", "Can Change City"),
            ("list_city", "Can View City List"),

            ("add_package", "Can Create Package"),
            ("view_package", "Can View Single Package"),
            ("change_package", "Can Change Package"),
            ("list_package", "Can View Package List"),

            ("add_extra", "Can Create Extra"),
            ("view_extra", "Can View Single Extra"),
            ("change_extra", "Can Change Extra"),
            ("list_extra", "Can View Extra List"),

            ("add_setting", "Can Create Setting"),
            ("view_setting", "Can View Single Setting"),
            ("change_setting", "Can Change Setting"),
            ("list_setting", "Can View Setting List"),

            ("add_profile", "Can Create Profile"),
            ("view_profile", "Can View Single Profile"),
            ("change_profile", "Can Change Profile"),
            ("list_profile", "Can View Profile List"),

            ("add_admin", "Can Create Admin"),
            ("view_admin", "Can View Single Admin"),
            ("change_admin", "Can Change Admin"),
            ("list_admin", "Can View Admin List"),

            ("add_log", "Can Create Log"),
            ("view_log", "Can View Single Log"),
            ("change_log", "Can Change Log"),
            ("list_log", "Can View Log List"),

        )


class User(AbstractUser):
    CREATED = 0
    ACTIVE = 1
    BANNED = 2
    KICKED = 3
    UPGRADE = 4
    STS = (
        (CREATED, 'Just Created'),
        (ACTIVE, 'Activated'),
        (BANNED, 'Disabled'),
        (KICKED, 'Disabled'),
        (UPGRADE, 'Active'),
     )

    WEB = 0
    ANDROID = 1
    IOS = 2
    TV = 3
    API = 4
    SOURCE = (
        (WEB, 'Web'),
        (ANDROID, 'Android'),
        (IOS, 'Ios'),
        (TV, 'Tv'),
        (API, 'Api'),

     )

    TEN_MINUTE = 0
    HALF_HOUR = 1
    ONE_HOUR = 2
    SIX_HOUR = 3
    ONE_DAY = 4
    FOUR_DAY = 5
    ONE_WEEK = 6
    ONE_MONTH = 7
    FOREVER = 8
    BAN_UNTIL = (
        (TEN_MINUTE, '10 Min'),
        (HALF_HOUR, '30 Min'),
        (ONE_HOUR, '1 Hour'),
        (SIX_HOUR, '6 Hour'),
        (ONE_DAY, '1 Day'),
        (FOUR_DAY, '4 Day'),
        (ONE_WEEK, '1 Week'),
        (ONE_MONTH, '1 Month'),
        (FOREVER, 'Forever'),

     )

    name = models.CharField(max_length=128, default='Newborn')
    mail = models.EmailField(max_length=75, default='darzi@protonmail.com', blank=True, null=True)
    cell_phone = models.CharField(max_length=14, blank=True, null=True)
    nationality = models.CharField(max_length=32, default='iran', blank=True, null=True)
    national_code = models.IntegerField(blank=True, null=True)
    post_code = models.IntegerField(blank=True, null=True)
    scan_identity = models.ImageField(upload_to='', storage='',  max_length=255, blank=True, null=True)
    scan_credit = models.ImageField(upload_to='', storage='',  max_length=255, blank=True, null=True)
    id_plan = models.IntegerField(blank=True, null=True)
    amount = models.DecimalField(decimal_places=3, max_digits=10, blank=True, null=True)
    used_kw = models.TextField(blank=True, null=True)
    used_city = models.TextField(blank=True, null=True)
    status = models.IntegerField(choices=STS, default=CREATED, blank=True, null=True)
    ban_until = models.IntegerField(choices=BAN_UNTIL, default=ONE_HOUR, blank=True, null=True)
    last_ip = models.GenericIPAddressField(blank=True, null=True)
    last_agent = models.TextField(blank=True, null=True)
    session_id = models.CharField(max_length=128, blank=True, null=True)
    reg_source = models.IntegerField(choices=SOURCE, default=WEB, blank=True, null=True)
    last_source = models.IntegerField(choices=SOURCE, default=WEB, blank=True, null=True)
    remained_days = models.IntegerField(blank=True, null=True)
    reached_limits = models.BooleanField(default=False)
    kw_remained = models.IntegerField(blank=True, null=True)
    city_remained = models.IntegerField(blank=True, null=True)
    result_remained = models.IntegerField(blank=True, null=True)






    # DISABLE = False
    # ACTIVE = True
    # STS = (
    #     (DISABLE, 'Disabled'),
    #     (ACTIVE, 'Active'),
    #  )
    # USERNAME_FIELD = 'username'
    # REQUIRED_FIELDS = []
    # is_anonymous = models.BooleanField(default=False)
    # # is_authenticated = models.BooleanField(default=False)
    # name = models.CharField(max_length=255, default='noname')
    #
    # mail_verify = models.CharField(max_length=255, choices=MAIL, default=DISABLE, blank=True, null=True)

# class Meta:
    #     permissions = [
    #         #
    #         # ("add_user", "Can Create User"),
    #         # ("view_user", "Can View Single User"),
    #         # ("change_user", "Can Change User"),
    #         ("list_user", "Can View User List"),
    #
    #         ("add_file", "Can Create File"),
    #         ("view_file", "Can View Single File"),
    #         ("change_file", "Can Change File"),
    #         ("list_file", "Can View File List"),
    #
    #         ("add_message", "Can Create Message"),
    #         ("view_message", "Can View Single Message"),
    #         ("change_message", "Can Change Message"),
    #         ("list_message", "Can View Message List"),
    #
    #         ("add_slide", "Can Create Slide"),
    #         ("view_slide", "Can View Single Slide"),
    #         ("change_slide", "Can Change Slide"),
    #         ("list_slide", "Can View Slide List"),
    #
    #         ("add_sponser", "Can Create Sponser"),
    #         ("view_sponser", "Can View Single Sponser"),
    #         ("change_sponser", "Can Change Sponser"),
    #         ("list_sponser", "Can View Sponser List"),
    #
    #         ("add_workshop", "Can Create Workshop"),
    #         ("view_workshop", "Can View Single Workshop"),
    #         ("change_workshop", "Can Change Workshop"),
    #         ("list_workshop", "Can View Workshop List"),
    #
    #         ("add_content", "Can Create Content"),
    #         ("view_content", "Can View Single Content"),
    #         ("change_content", "Can Change Content"),
    #         ("list_content", "Can View Content List"),
    #
    #         ("add_link", "Can Create Link"),
    #         ("view_link", "Can View Single Link"),
    #         ("change_link", "Can Change Link"),
    #         ("list_link", "Can View Link List"),
    #
    #         ("add_page", "Can Create Page"),
    #         ("view_page", "Can View Single Page"),
    #         ("change_page", "Can Change Page"),
    #         ("list_page", "Can View Page List"),
    #
    #         ("add_setting", "Can Create Setting"),
    #         ("view_setting", "Can View Single Setting"),
    #         ("change_setting", "Can Change Setting"),
    #         ("list_setting", "Can View Setting List"),
    #
    #         ("add_article", "Can Create Article"),
    #         ("view_article", "Can View Single Article"),
    #         ("change_article", "Can Change Article"),
    #         ("list_article", "Can View Article List"),
    #
    #         ("view_self_article_list", "View Self Articles List"),
    #         ("view_self_article", "View Self Single Article"),
    #         ("send_self_article", "Send Self Article"),
    #         ("change_self_article", "Change Self Article"),
    #         ("delete_self_article", "Delete Self Article"),
    #
    #         ("view_db_article_list", "View Dabir Articles List"),
    #         ("view_db_article", "View Dabir Single Article"),
    #         ("send_db_article", "Send Dabir Article"),
    #         ("change_db_article", "Change Dabir Article"),
    #         ("delete_db_article", "Delete Dabir Article"),
    #
    #
    #         ("view_dv_article_list", "View Davar Articles List"),
    #         ("view_dv_article", "View Davar Single Article"),
    #         ("send_dv_article", "Send Davar Article"),
    #         ("change_dv_article", "Change Davar Article"),
    #         ("delete_dv_article", "Delete Davar Article"),
    #
    #
    #
    #     ]

    # class Meta: ???
    #     verbose_name = 'Register'
    #     verbose_name_plural = 'Registers'

    # def __str__(self):
    #     return self.name

    # helps

    # imports
    #  from django.db import models
    #  from blog.models import Blog
    # # create
    #  b = Blog(name='Beatles Blog', tagline='All the latest Beatles news.')
    #  b.save()
    # # update
    #  b5.name = 'New name'
    #  b5.save()
    #
    # # select
    #  all_entries = Entry.objects.all()
    #
    #  Blog.objects
    #
    #  b = Blog(name='Foo', tagline='Bar')
    #  b.objects
    #
    # # filters
    # Entry.objects.filter(pub_date__year=2006)
    # Entry.objects.all().filter(pub_date__year=2006)
    #  Entry.objects.filter(
    #      headline__startswith='What'
    #  ).exclude(
    #      pub_date__gte=datetime.date.today()
    #  ).filter(
    #      pub_date__gte=datetime.date(2005, 1, 30)
    #  )
    # # get one
    #  one_entry = Entry.objects.get(pk=1)
    #  Entry.objects.filter(blog_id=4)
    #  Entry.objects.all()[:5]
    # # like
    # Entry.objects.get(headline__contains='Lennon')
    # # delete
    #  e.delete()
    #  Entry.objects.filter(pub_date__year=2005).delete()
    # b = Blog.objects.get(pk=1)
    # # This will delete the Blog and all of its Entry objects.
    # b.delete()
    # Entry.objects.all().delete()
