INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'numberscrapper',
    'drf_yasg',
    'rest_framework_swagger',
    # 'rest_framework_jwt',
    "api",
    "anymail",
    'geo',
    "users",
    'payments',
    "forum",
    'django_common'
]