# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases
# https://pypi.org/project/mysqlclient/
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'dj32',
        'USER': 'root',
        'PASSWORD': '1234567890',
        'HOST': 'localhost',
        'PORT': '3306',
        'OPTIONS': {
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
        },


    }
}
