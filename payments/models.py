from django.db import models
from users.models import User
from geo.models import City
# Create your models here.


class Plan(models.Model):
    DISABLE = 0
    ACTIVE = 1

    STS = (
        (DISABLE, 'Disabled'),
        (ACTIVE, 'Activated'),

     )

    name = models.CharField(max_length=128, blank=True, null=True)
    price = models.DecimalField(decimal_places=3, max_digits=10, blank=True, null=True)
    sells = models.IntegerField(blank=True, null=True)
    status = models.IntegerField(choices=STS, default=ACTIVE, blank=True, null=True)
    offer = models.CharField(max_length=128, blank=True, null=True)
    offer_until = models.CharField(max_length=128, blank=True, null=True)
    kw_limit = models.CharField(max_length=128, blank=True, null=True)
    city_limit = models.CharField(max_length=128, blank=True, null=True)
    result_limit = models.CharField(max_length=128, blank=True, null=True)
    days = models.CharField(max_length=3, blank=True, null=True)


class Discount(models.Model):
    PERCENT = 1
    AMOUNT = 2

    TYPE = (
        (PERCENT, '%'),
        (AMOUNT, '$'),

     )

    DISABLE = 0
    ACTIVE = 1

    STS = (
        (DISABLE, 'Disabled'),
        (ACTIVE, 'Activated'),

    )

    type = models.IntegerField(choices=TYPE, default=PERCENT, blank=True, null=True)
    # 'max_length' is ignored when used with IntegerField.
    # HINT: Remove 'max_length' from field
    expired_on = models.CharField(max_length=32, default='1399-12-01', blank=True, null=True)
    status = models.CharField(max_length=1, choices=STS, default=ACTIVE, blank=True, null=True)
    value = models.IntegerField(blank=True, null=True)
    code = models.CharField(max_length=32, blank=True, null=True)
    text = models.TextField(blank=True, null=True)
    landing = models.BooleanField(default=False, blank=True, null=True)


class Payment(models.Model):
    PLAN = 0
    PACKAGE = 0
    SMS = 0
    EXTRA = 0

    TYPE = (
        (PLAN, 'Disabled'),
        (PACKAGE, 'Disabled'),
        (SMS, 'Disabled'),
        (EXTRA, 'Disabled'),

    )
    UNPAID = 0
    SUCCESS = 1
    CANCELLED = 2
    LOW_MONEY = 3
    ERROR = 4
    FRAUD = 5

    STS = (
        (UNPAID, 'UnPaid'),
        (SUCCESS, 'Success'),
        (CANCELLED, 'Cancelled'),
        (LOW_MONEY, 'Low Money'),
        (ERROR, 'Error'),
        (FRAUD, 'Fraud'),

    )

    type = models.IntegerField(choices=TYPE, default=PLAN, blank=True, null=True)
    id_user = models.ForeignKey(User, on_delete=models.CASCADE, max_length=11, blank=True, null=True)
    id_user_plan = models.ForeignKey(Plan, on_delete=models.CASCADE, max_length=11, blank=True, null=True)
    paid_amount = models.DecimalField(decimal_places=3, max_digits=10, blank=True, null=True)
    ip = models.GenericIPAddressField(blank=True, null=True)
    agent = models.TextField(blank=True, null=True)
    status = models.IntegerField(choices=STS, default=UNPAID, blank=True, null=True)
    id_discount = models.ForeignKey(Discount, on_delete=models.CASCADE, max_length=11, blank=True, null=True)
    callback = models.TextField(blank=True, null=True)
    recheck = models.BooleanField(default=False, blank=True, null=True)
    verify = models.BooleanField(default=False, blank=True, null=True)
    datetime = models.DateTimeField(max_length=64, blank=True, null=True)


class Package(models.Model):
    DISABLE = 0
    ACTIVE = 1

    STS = (
        (DISABLE, 'Disabled'),
        (ACTIVE, 'Activated'),

    )

    name = models.CharField(max_length=128, blank=True, null=True)
    cork = models.CharField(max_length=1, blank=True, null=True)  # value ( category or keyword )
    city = models.ForeignKey(City, on_delete=models.CASCADE, max_length=128, blank=True, null=True)
    sells = models.IntegerField(blank=True, null=True)
    price = models.IntegerField(blank=True, null=True)
    status = models.CharField(max_length=1, choices=STS, default=ACTIVE, blank=True, null=True)
    # img = models.CharField(upload_to='', storage='', blank=True, null=True)
    # poster = models.FileField(upload_to='', storage='', blank=True, null=True)
    img = models.CharField(max_length=128, blank=True, null=True)
    poster = models.FileField(max_length=128, blank=True, null=True)
    landing = models.BooleanField(blank=True, null=True)
    text = models.TextField(blank=True, null=True)
    csv = models.FileField(upload_to='', storage='', blank=True, null=True)
    xls = models.FileField(upload_to='', storage='', blank=True, null=True)
    txt = models.FileField(upload_to='', storage='', blank=True, null=True)




