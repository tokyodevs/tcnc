from django.db import models

# Create your models here.


class TelegramService(models.Model):
    DISABLE = 0
    ACTIVE = 1

    STS = (
        (DISABLE, 'Disabled'),
        (ACTIVE, 'Activated'),

    )

    contacts = models.IntegerField(max_length=5, blank=True, null=True)
    usage = models.IntegerField(max_length=11, blank=True, null=True)
    daily_usage = models.IntegerField(max_length=11, blank=True, null=True)
    who_used = models.TextField(blank=True, null=True)
    status = models.IntegerField(max_length=1, choices=STS, default=ACTIVE, blank=True, null=True)
    last_usage = models.DateTimeField(max_length=64, blank=True, null=True)
    config_file = models.FileField(upload_to='', storage='', blank=True, null=True)
    # config_file = models.CharField(max_length=255, blank=True, null=True)


class SmsService(models.Model):
    DISABLE = 0
    ACTIVE = 1

    STS = (
        (DISABLE, 'Disabled'),
        (ACTIVE, 'Activated'),

    )

    username = models.CharField(max_length=128, blank=True, null=True)
    password = models.CharField(max_length=128, blank=True, null=True)
    token = models.CharField(max_length=512, blank=True, null=True)
    lines = models.TextField(blank=True, null=True)
    default_line = models.CharField(max_length=20, blank=True, null=True)
    server = models.GenericIPAddressField(max_length=128, blank=True, null=True)
    usage = models.IntegerField(max_length=11, blank=True, null=True)
    daily_usage = models.IntegerField(max_length=11, blank=True, null=True)
    who_used = models.TextField(blank=True, null=True)
    status = models.IntegerField(max_length=1, choices=STS, default=ACTIVE, blank=True, null=True)
    last_usage = models.DateTimeField(max_length=64, blank=True, null=True)
