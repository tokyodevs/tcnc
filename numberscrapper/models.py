from django.db import models
from users.models import User
from geo.models import City
from payments.models import Plan

# Create your models here.


class SMS(models.Model):
    id_user = models.ForeignKey(User, on_delete=models.CASCADE, max_length=128, blank=True, null=True)
    id_user_plan = models.ForeignKey(Plan, on_delete=models.CASCADE, max_length=128, blank=True, null=True)
    text = models.CharField(max_length=128, blank=True, null=True)
    each_price = models.CharField(max_length=128, blank=True, null=True)
    total_numbers = models.CharField(max_length=128, blank=True, null=True)
    total_price = models.CharField(max_length=128, blank=True, null=True)


class SmsNumber(models.Model):
    sms_id = models.CharField(max_length=128, blank=True, null=True)
    mobile = models.CharField(max_length=128, blank=True, null=True)
    id_user = models.ForeignKey(User, on_delete=models.CASCADE, max_length=128, blank=True, null=True)
    id_user_plan = models.ForeignKey(Plan, on_delete=models.CASCADE, max_length=128, blank=True, null=True)


class SearchedKeyword(models.Model):
    id_user = models.ForeignKey(User, on_delete=models.CASCADE,max_length=11, blank=True, null=True)
    id_user_plan = models.ForeignKey(Plan, on_delete=models.CASCADE,max_length=11, blank=True, null=True)
    datetime = models.DateTimeField(max_length=64, blank=True, null=True)
    text = models.TextField(blank=True, null=True)


class SearchedCities(models.Model):
    id_user = models.ForeignKey(User, on_delete=models.CASCADE,max_length=11, blank=True, null=True)
    id_user_plan = models.ForeignKey(Plan, on_delete=models.CASCADE,max_length=11, blank=True, null=True)
    datetime = models.DateTimeField(max_length=64, blank=True, null=True)
    text = models.CharField(max_length=128, blank=True, null=True)
    id_city = models.ForeignKey(City, on_delete=models.CASCADE, max_length=11, blank=True, null=True)


class LiveSearch(models.Model):
    id_user = models.ForeignKey(User, on_delete=models.CASCADE, max_length=11, blank=True, null=True)
    id_user_plan = models.ForeignKey(Plan, on_delete=models.CASCADE,max_length=11, blank=True, null=True)
    id_city = models.ForeignKey(City, on_delete=models.CASCADE,max_length=11, blank=True, null=True)
    id_keyword = models.ForeignKey(SearchedKeyword, on_delete=models.CASCADE, max_length=11, blank=True, null=True)
    datetime = models.DateTimeField(max_length=64, blank=True, null=True)


class LiveResult(models.Model):
    id_user = models.ForeignKey(User, on_delete=models.CASCADE, max_length=11, blank=True, null=True)
    id_search = models.ForeignKey(LiveSearch, on_delete=models.CASCADE, max_length=11, blank=True, null=True)  # main one
    id_user_plan = models.ForeignKey(Plan, on_delete=models.CASCADE, max_length=11, blank=True, null=True)
    id_city = models.ForeignKey(City, on_delete=models.CASCADE, max_length=11, blank=True, null=True)
    id_keyword = models.ForeignKey(SearchedKeyword, on_delete=models.CASCADE, max_length=11, blank=True, null=True)
    cell_phone = models.CharField(max_length=14, blank=True, null=True)
    datetime = models.DateTimeField(max_length=64, blank=True, null=True)


class Search(models.Model):
    id_user = models.ForeignKey(User, on_delete=models.CASCADE, max_length=11, blank=True, null=True)
    id_user_plan = models.ForeignKey(Plan, on_delete=models.CASCADE, max_length=11, blank=True, null=True)
    id_city = models.ForeignKey(City, on_delete=models.CASCADE, max_length=11, blank=True, null=True)
    id_keyword = models.ForeignKey(SearchedKeyword, on_delete=models.CASCADE, max_length=11, blank=True, null=True)
    datetime = models.DateTimeField(max_length=64, blank=True, null=True)


class Result(models.Model):
    id_user = models.ForeignKey(User, on_delete=models.CASCADE, max_length=11, blank=True, null=True)
    id_search = models.ForeignKey(LiveSearch, on_delete=models.CASCADE, max_length=11, blank=True, null=True)  # main one
    id_user_plan = models.ForeignKey(Plan, on_delete=models.CASCADE, max_length=11, blank=True, null=True)
    id_city = models.ForeignKey(City, on_delete=models.CASCADE, max_length=11, blank=True, null=True)
    id_keyword = models.ForeignKey(SearchedKeyword, on_delete=models.CASCADE, max_length=11, blank=True, null=True)
    result = models.TextField(blank=True, null=True)
    datetime = models.DateTimeField(max_length=64, blank=True, null=True)


