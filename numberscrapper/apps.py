from django.apps import AppConfig


class NumberscrapperConfig(AppConfig):
    name = 'numberscrapper'
