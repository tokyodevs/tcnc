from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from unidecode import unidecode

executable_path = '/Users/mac/important/chromedriver'

options = webdriver.ChromeOptions()
options.add_argument("headless")
driver = webdriver.Chrome(executable_path=executable_path, options=options)
cities = {
    "cityname": "https://domain/s/cityname",

}
selected_city = cities['city_name']
searched_keyword = 'cat_name'


def examinated_records(size):
    records = 0
    page_sizes = {"max": 10000, "custom": 15, "pages": 0, "demo": 3}
    for i in page_sizes:
        if size == i:
            records = page_sizes[size]

    return records


sleep_size = 1
scroll_size = examinated_records("demo")
driver.get(selected_city)
# assert "Python" in driver.title
# elem = driver.find_element_by_name("q")
elem = driver.find_element_by_xpath('//*[@id="react-select-search-bar-input"]')
# elem.clear()
elem.send_keys(searched_keyword)
elem.send_keys(Keys.RETURN)

for i in range(scroll_size):
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    time.sleep(sleep_size)

result_list = driver.find_elements_by_class_name("post-card")
# result_size = len(result_list)
# for elem in result_list:


# driver.close()
stopper = 200
stopper_count = 0
numbers = []
for link in result_list:
    options = webdriver.ChromeOptions()
    options.add_argument("headless")
    driver = webdriver.Chrome(executable_path=executable_path, options=options)

    driver.get(link.get_attribute("href"))
    driver.find_element_by_xpath('/html/body/div[1]/div[2]/div/div[3]/div[2]/div/button').click()
    driver.find_element_by_xpath('/html/body/div[4]/div/div/div[3]/button').click()

    time.sleep(5)
    try:
        number = driver.find_element_by_xpath('/html/body/div[1]/div[2]/div/div[3]/div[3]/div[1]/div[1]/div/a').text
        number = unidecode(number)
        numbers.append(number)
    except:
        number = 'hidden'
    # time.sleep(5)
    print(number)
    stopper_count += 1
    # driver.close()
    if stopper == stopper_count:
        break

driver.close()
print(numbers)


