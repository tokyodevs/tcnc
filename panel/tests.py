from django.test import TestCase
from panel.models import User
# global str
# Create your tests here.


class UserTestCase(TestCase):
    def setUp(self):
        User.objects.create(name="lion", status=True)
        User.objects.create(name="cat", status=False)

    def test_users_can_status(self):
        """Users that can status are correctly identified"""
        lion = User.objects.get(name="lion")
        cat = User.objects.get(name="cat")
        # return lion.status()
        self.assertEqual(lion.status,True)
        self.assertEqual(cat.status,False)





        # from django.test import TestCase
        # from panel.models import User
        # # global str
        # # Create your tests here.
        #
        #
        # class UserTestCase(TestCase):
        #     def setUp(self):
        #         User.objects.create(name="lion", major="roar")
        #         User.objects.create(name="cat", major="meow")
        #
        #     def test_users_can_major(self):
        #         """Users that can status are correctly identified"""
        #         lion = User.objects.get(name="lion")
        #         cat = User.objects.get(name="cat")
        #         # return lion.status()
        #         self.assertEqual(lion.major,"roar")
        #         self.assertEqual(cat.major,"meow")
