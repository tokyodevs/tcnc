# import the logging library
import logging
from urllib import request

logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'console': {
            'format': '%(name)-12s %(levelname)-8s %(message)s'
        },
        'file': {
            'format': '%(asctime)s %(name)-12s %(levelname)-8s %(message)s'
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'console'
        },
        'file': {
            'level': 'ERROR',
            'class': 'logging.FileHandler',
            'formatter': 'file',
            'filename': 'debug.log'
        }
    },
    'loggers': {
        '': {
            'level': 'ERROR',
            'handlers': ['console', 'file']
        },
		'django.request': {
	   'level': 'ERROR',
	   'handlers': ['console', 'file']
   }
    }
})

# This retrieves a Python logging instance (or creates it)
logger = logging.getLogger(__name__)

class THCMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        # Log an error message
        # logger.error(request.site)
        # logger.error(request.META['HTTP_REFERER']) #what if null
        # 1.0.0.127.in-addr.arpa == local
        #
        # logger.error(request.get_port()) #8000
        # logger.error(request.META['HTTP_HOST']) #host:8000
        # logger.error(request.META['HTTP_USER_AGENT']) #ua
        # logger.error(request.META['QUERY_STRING']) # null if null
        # logger.error(request.META['REMOTE_ADDR']) #userip
        # logger.error(request.META['REMOTE_HOST']) #null if local
        # logger.error(request.META['SERVER_NAME']) #this is our server ?
        # logger.error(request.is_secure()) #true or false
        # logger.error(request.user) ## anyno admin
        # logger.error(request.scheme) #http
        # logger.error(request.headers) #+++csrftoken+sessionid
        # logger.error(request.body)#b''
        # logger.error(request.path)#/panel/login
        # logger.error(request.method) #GET

        # logger.error(request.session['is_authenticated']) #GET

        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response



        #
        # debug,info,warning,error,critical,exception
        # # A string with a variable at the "info" level
        # logger.info("The value of var is %s", var)
        #
        # # Logging the traceback for a caught exception
        # try:
        #     function_that_might_raise_index_error()
        # except IndexError:
        #     # equivalent to logger.error(msg, exc_info=True)
        #     logger.exception("Something bad happened")
        #
        #
