from pywebcopy import save_website

dir_name = 'login'
site_url = 'http://brandio.io/envato/iofrm/html/'
kwargs = {'project_name': dir_name}
save_website(
    url=site_url,
    project_folder=dir_name,
    **kwargs
)
