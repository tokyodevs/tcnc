from django.db import models

# Create your models here.


class City(models.Model):
    DISABLE = 0
    ACTIVE = 1

    STS = (
        (DISABLE, 'Disabled'),
        (ACTIVE, 'Activated'),

    )

    name = models.CharField(max_length=128, blank=True, null=True)
    province = models.CharField(max_length=128, blank=True, null=True)
    status = models.CharField(max_length=128, choices=STS, default=ACTIVE, blank=True, null=True)
    usage = models.CharField(max_length=128, blank=True, null=True)
    poster = models.CharField(max_length=128, blank=True, null=True)
    text = models.CharField(max_length=128, blank=True, null=True)


class Province(models.Model):

    DISABLE = 0
    ACTIVE = 1

    STS = (
        (DISABLE, 'Disabled'),
        (ACTIVE, 'Activated'),

    )

    name = models.CharField(max_length=128, blank=True, null=True)
    status = models.CharField(max_length=128, choices=STS, default=ACTIVE, blank=True, null=True)
    usage = models.CharField(max_length=128, blank=True, null=True)
    poster = models.CharField(max_length=128, blank=True, null=True)
    text = models.CharField(max_length=128, blank=True, null=True)
