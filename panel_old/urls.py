from django.urls import path
from . import views
# from django.views.decorators.cache import cache_page

urlpatterns = [
    path('', views.index, name='index'),
    path('my_articles', views.my_articles, name='my_articles'),
    # path('my_articles/add', views.my_articles, name='my_articles'),
    path('login', views.login_page, name='login'),
    path('logout', views.log_out, name='log_out'),
    path('change_password', views.change_password, name='change_password'),
    path('inbox', views.inbox, name='inbox'),
    path('inbox/read/<int:mid>', views.read, name='read'),
]
