from django.shortcuts import render, redirect, render, get_object_or_404
from django.views.generic.base import RedirectView
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import Permission, User, Group
from django.contrib.auth.hashers import check_password
from django.contrib.contenttypes.models import ContentType
from django.views.decorators.cache import never_cache, cache_control
from django.http import HttpResponse
from urllib import request
from pprint import pprint
# from .message import MessageSystem
import requests
import logging
from django.core.mail import send_mail, EmailMultiAlternatives
from anymail.message import attach_inline_image_file

# Get an instance of a logger
logger = logging.getLogger(__name__)


# return HttpResponse(str(var))

# Create your views here.
def isset(variable):
    return variable in locals() or variable in globals()


# return HttpResponse(str(isset(var)))
def curl(request):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsIn',
    }

    response = requests.get('', headers=headers)
    context = {"context_variable": response.text}
    return render(request, 'panel/index.html', context)


#  tpl+var
@never_cache
def index(request):
    # if 'is_authenticated' in request.session:
    if request.user.is_authenticated:
        # logger.error(request.user.type)
        # send_mail("It works!", "This will get sent through Mailgun","Anymail Sender <frosm@example.com>", ["x@gmail.com"])
        # if(request.user.type=="US"):
        # my_group = Group.objects.get(name='user')
        # my_group.user_set.add(request.user)
        # request.user.groups.clear()
        context = {
            "context_variable": 'not logged in yet',
            "name": request.user.name,
            "username": request.user.username,
            'cml': 'sss'
        }

        return render(request, 'panel/index.html', context)

    return redirect('/panel/login')


@never_cache
@login_required()
@permission_required('panel.change_workshop', raise_exception=True)
def my_articles(request):
    # my_group = Group.objects.get(name='user')
    # request.user.groups.clear()
    context = {"context_variable": 'not logged in yet'}
    return render(request, 'panel/v.html', context)


@never_cache
@login_required()
def change_password(request):
    if request.method == 'GET':
        context = {
            "name": request.user.name,
            "username": request.user.username,
            "email": request.user.email,
            "password": request.user.password,
            "pic": request.user.pic,
            "family": request.user.family,
        }
        response = HttpResponse()
        response['Cache-Control'] = 'no-cache'
        return render(request, 'panel/change_password.html', context)
    if request.method == 'POST':
        u = User.objects.get(username=request.user.username)
        opassword = request.POST.get('opassword', default=None)
        password = request.POST.get('password', default=None)
        rpassword = request.POST.get('rpassword', default=None)
        if check_password(opassword, u.password):
            if rpassword == password:
                u.set_password(password)
                u.save()
                error = 'changed successfully'
            else:
                error = 'pass and repeat not equal'
        else:
            error = 'old pass was wrong'

        context = {
            "error": error,
            "name": request.user.name,
            # "username": request.user.username,
            # "email": request.user.email,
            # "password": request.user.password,
            # "pic": request.user.pic,
            # "family": request.user.family,
        }
        return render(request, 'panel/change_password.html', context)


@never_cache
@login_required()
def inbox(request):
    if request.method == 'GET':
        list = Message.objects.all().filter(recipient_id=request.user.id).order_by('-id')
        context = {
            "name": request.user.name,
            "username": request.user.username,
            "email": request.user.email,
            "password": request.user.password,
            "pic": request.user.pic,
            "family": request.user.family,
            "list": list,
        }
        response = HttpResponse()
        response['Cache-Control'] = 'no-cache'
        return render(request, 'panel/inbox.html', context)


@never_cache
@login_required()
def read(request, mid=1):
    if request.method == 'GET':
        myid = int(mid)
        list = Message.objects.all().filter(recipient_id=request.user.id).order_by('-id')
        list_inner = Message.objects.all().filter(recipient_id=request.user.id).filter(thread_id=mid).order_by('-id')

        msg = Message.objects.filter(id=myid)
        # msg=Message.objects.filter(blog_id=4)filter(id=request.GET.get('id'))
        context = {
            "name": request.user.name,
            "username": request.user.username,
            "email": request.user.email,
            "password": request.user.password,
            "pic": request.user.pic,
            "family": request.user.family,
            "list": list,
            "msg": msg,
            "myid": myid,
            "list_inner": list_inner,
        }
        response = HttpResponse()
        response['Cache-Control'] = 'no-cache'
        return render(request, 'panel/read.html', context)


@never_cache
def login_page(request):
    # cache.clear()
    # cache.close()
    if request.method == 'GET':
        context_variable = '876543'
        context = {"context_variable": context_variable}
        response = HttpResponse()
        response['Cache-Control'] = 'no-cache'
        return render(request, 'panel/login.html', context)
    if request.method == 'POST':
        username = request.POST.get('username', default=None)
        password = request.POST.get('password', default=None)
        user = authenticate(username=username, password=password)
        if user is not None:
            # A backend authenticated the credentials
            # Redirect to a success page.
            login(request, user)
            # user.user_permissions.set([permission_list])
            # logger.error(user.type)
            # request.session['is_authenticated']='login'

            return redirect('/panel')


        else:
            redirect('login')

        #     # No backend authenticated the credentials
        #     # Return an 'invalid login' error message.

        context_variable = ';'
        context = {"context_variable": context_variable}
        return render(request, 'panel/login.html', context)


@never_cache
def log_out(request):
    # del request.session['is_authenticated']
    print('logged out')
    logout(request)
    return redirect('login')

# #  tpl+var
# @never_cache
# @login_required()
# @permission_required('panel.change_workshop', raise_exception=True)
# def my_articles(request):
#         # logger.error(request.user.type)
#         # if(request.user.type=="US"):
#         # my_group.user_set.add(request.user)
#         my_group = Group.objects.get(name='user')
#         request.user.groups.clear()
#         context = {"context_variable": 'not logged in yet'}
#         return render(request,'panel/v.html', context)
#
#


# #  tpl+var
# @never_cache
# @login_required()
# @permission_required('panel.change_workshop', raise_exception=True)
# def my_articles(request):
#     # if 'is_authenticated' in request.session:
#     # if request.user.is_authenticated:
#         # logger.error(request.user.type)
#         # if(request.user.type=="US"):
#         my_group = Group.objects.get(name='user')
#         # my_group.user_set.add(request.user)
#         request.user.groups.clear()
#         context = {"context_variable": 'not logged in yet'}
#         return render(request,'panel/v.html', context)
#
#     # return redirect('/panel/login')
#
#


# #  tpl+var
# def index(request):
#     	context = {"context_variable": context_variable}
#         return render(request,'index.html', context)
#

# static page
#
# def index(request):
#     return render(request,'index.html')
#

# pure HttpResponse
# from django.http import HttpResponse
#
# def index(request):
#     return HttpResponse("Hello, World!")


# {% load static %}


#
# {% load cache %}
# {% cache 500 sidebar %}
#     .. sidebar ..
# {% endcache %}
# <!-- <a href="{{ url('panel:index') }}">Administration</a> -->


# msg = EmailMultiAlternatives(
#     subject="Please activate your account",
#     body="Click to activate your account: http://example.com/activate",
#     from_email="Example <admin@example.com>",
#     to=["New User <user1@example.com>", "account.manager@example.com"],
#     reply_to=["Helpdesk <support@example.com>"])

# # Include an inline image in the html:
# logo_cid = attach_inline_image_file(msg, "/path/to/logo.jpg")
# html = """<img alt="Logo" src="cid:{logo_cid}">
#           <p>Please <a href="http://example.com/activate">activate</a>
#           your account</p>""".format(logo_cid=logo_cid)
# msg.attach_alternative(html, "text/html")

# # Optional Anymail extensions:
# msg.metadata = {"user_id": "8675309", "experiment_variation": 1}
# msg.tags = ["activation", "onboarding"]
# msg.track_clicks = True

# # Send it:
# msg.send()