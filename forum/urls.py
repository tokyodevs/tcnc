
from django.conf.urls import *
from . import views

urlpatterns = (

    url('^post/$', views.post_list, name='post-list'),
    url('^post/<int:id>/$', views.post_details, name='post-details'),
    url('^post/<int:id>/delete/$', views.post_delete, name='post-delete'),
    url('^topic/$', views.topic_list, name='topic-list'),
    url('^topic/<int:id>/$', views.topic_details, name='topic-details'),
    url('^topic/<int:id>/delete/$', views.topic_delete, name='topic-delete'),
    url('^forum/$', views.forum_list, name='forum-list'),
    url('^forum/<int:id>/$', views.forum_details, name='forum-details'),
    url('^forum/<int:id>/delete/$', views.forum_delete, name='forum-delete'),

                       )
