from django import forms


from forum.models import Forum

class ForumForm(forms.ModelForm):
    class Meta:
        model = Forum
        fields = "__all__"

from forum.models import Topic

class TopicForm(forms.ModelForm):
    class Meta:
        model = Topic
        fields = "__all__"

from forum.models import Post

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = "__all__"