from django.db import models

# Create your models here.
from users.models import User


class Forum(models.Model):
    
    name = models.CharField(max_length=255, null=True, blank=True)

    update_date = models.DateTimeField(auto_now=True)
    create_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-id']

# from django.contrib.auth.models import User, Group
from forum.models import Forum
class Topic(models.Model):
    
    created_by = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)

    title = models.CharField(max_length=255, null=True, blank=True)

    forum = models.ForeignKey(Forum, null=True, blank=True,on_delete=models.CASCADE)

    update_date = models.DateTimeField(auto_now=True)
    create_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-id']
from forum.models import Topic
class Post(models.Model):
    
    title = models.CharField(max_length=255, null=True, blank=True)

    body = models.TextField(null=True, blank=True)

    created_by = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)

    topic = models.ForeignKey(Topic, null=True, blank=True, on_delete=models.CASCADE)

    update_date = models.DateTimeField(auto_now=True)
    create_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-id']
